package Model

import file
import fileActualUser
import java.io.File
import java.nio.file.Files
import java.nio.file.Paths

class Judge {

    fun formatAnswer(string: String): String {
        // Set var to detect duplicate chars
        var prevChar = ' '
        return string.trim() // Remove surrounding spaces to simplify
            .replace(' ', '_') // Spaces into underscores
            .filter { char -> // Remove duplicated underscores
                if (char == '_' && prevChar == '_') false
                else {
                    prevChar = char
                    true
                }
            }
    }

    private fun problemsHistoric(userLineLogged: Int, UserExist: Boolean): String {
        var starsProblems = ""
        if (UserExist == true){
            val lineUserLogged = Files.readAllLines(Paths.get("$file"))[userLineLogged]
            val temp = lineUserLogged.split(";")
            starsProblems = temp[2]
        }
        if (UserExist == false){
            val fileUserData = File("./src/main/Utilities/UserData")
            val lastLine = fileUserData.readLines().last()
            val temp = lastLine.split(";")
            starsProblems = temp[2]
        }
        return starsProblems
    }

    fun checkPoints(userLineLogged: Int, UserExist: Boolean):MutableList<String>{
        var problem1Points= ""
        var problem2Points= ""
        var problem3Points= ""
        var problem4Points= ""
        var problem5Points= ""

        val listDataProblems = problemsHistoric(userLineLogged, UserExist)
        val formatedStars = listDataProblems.split(",")
        for (i in formatedStars) {
            if (i.isEmpty()) {

            } else {
                val actualProblem = i.split(":")
                val numberOfQuestion = actualProblem.first()
                val problemSolvedOrNot = actualProblem[1]
                val pointsOfQuestion = actualProblem[2]
                when (numberOfQuestion.toInt()) {
                    0->{
                        problem1Points = pointsOfQuestion
                    }
                    1->{
                        problem2Points = pointsOfQuestion
                    }
                    2->{
                        problem3Points = pointsOfQuestion
                    }
                    3->{
                        problem4Points = pointsOfQuestion
                    }
                    4->{
                        problem5Points = pointsOfQuestion
                    }
                }
            }
        }
        if (problem1Points == "") {
            problem1Points = "0"
        }
        if (problem2Points == "") {
            problem2Points = "0"
        }
        if (problem3Points == "") {
            problem3Points = "0"
        }
        if (problem4Points == "") {
            problem4Points = "0"
        }
        if (problem5Points == "") {
            problem5Points = "0"
        }
        return mutableListOf(problem1Points, problem2Points, problem3Points, problem4Points, problem5Points)
    }

    fun checkProblemsSolvedByUser(userLineLogged: Int, UserExist: Boolean): MutableList<String> {
        val listDataProblems = problemsHistoric(userLineLogged, UserExist) //☆,,,,
        var problem1 = ""
        var problem2 = ""
        var problem3 = ""
        var problem4 = ""
        var problem5 = ""

        val formatedStars = listDataProblems.split(",")
        for (i in formatedStars) {
            if (i.isEmpty()) {

            } else {
                val actualProblem = i.split(":")
                val numberOfQuestion = actualProblem[0]
                val problemSolvedOrNot = actualProblem[1]

                when (numberOfQuestion.toInt()) {
                    0 -> {
                        if (problemSolvedOrNot == "★") {
                            problem1 = "✅"
                        } else if (problemSolvedOrNot == "☆") {
                            problem1 = "❌"
                        }
                    }
                    1 -> {
                        if (problemSolvedOrNot == "★") {
                            problem2 = "✅"
                        } else if (problemSolvedOrNot == "☆") {
                            problem2 = "❌"
                        }
                    }
                    2 -> {
                        if (problemSolvedOrNot == "★") {
                            problem3 = "✅"
                        } else if (problemSolvedOrNot == "☆") {
                            problem3 = "❌"
                        }
                    }
                    3 -> {
                        if (problemSolvedOrNot == "★") {
                            problem4 = "✅"
                        } else if (problemSolvedOrNot == "☆") {
                            problem4 = "❌"
                        }
                    }
                    4 -> {
                        if (problemSolvedOrNot == "★") {
                            problem5 = "✅"
                        } else if (problemSolvedOrNot == "☆") {
                            problem5 = "❌"
                        }
                    }
                }
            }
        }
        if (problem1 == "") {
            problem1 = "❓"
        }
        if (problem2 == "") {
            problem2 = "❓"
        }
        if (problem3 == "") {
            problem3 = "❓"
        }
        if (problem4 == "") {
            problem4 = "❓"
        }
        if (problem5 == "") {
            problem5 = "❓"
        }
        return mutableListOf(problem1, problem2, problem3, problem4, problem5)
    }

    private fun factorial(num:Int): Int{
        val num = num
        var factorial = 1
        for (i in 1..num) {
            factorial *= i
        }
        return factorial
    }

    fun puntuation(trys: Int):Int{
        var basePoints = 100
        when (trys){
            1 -> basePoints *= factorial(4)
            2 -> basePoints *= factorial(3)
            3 -> basePoints *= factorial(2)
            4 -> basePoints *= factorial(1)
        }
        return basePoints
    }

    @Suppress("DEPRECATION")
    fun updateUserData(){
        val file = File("file.txt")
        val tempFile = createTempFile()
        val regex = Regex("""some line in file""")
        tempFile.printWriter().use { writer ->
            file.forEachLine { line ->
                writer.println(when {
                    regex.matches(line) -> line + "a some text I'd like to add after line"
                    else -> line
                })
            }
        }
        check(file.delete() && tempFile.renameTo(file)) { "failed to replace file" }
    }

    fun addStarsIfSolved(turn: Int,resultMutableList: MutableList<String>):MutableList<String>{
        when(turn){
            1 -> {resultMutableList.add(0,"★") }
            2 -> {resultMutableList.add(1,"★") }
            3 -> {resultMutableList.add(2,"★") }
            4 -> {resultMutableList.add(3,"★") }
            5 -> {resultMutableList.add(4,"★") }
        }
        return resultMutableList
    }

    fun addStarsIfNotSolved(turn: Int,resultMutableList: MutableList<String>):MutableList<String>{
        when(turn){
            1 -> {resultMutableList.add(0,"☆") }
            2 -> {resultMutableList.add(1,"☆") }
            3 -> {resultMutableList.add(2,"☆") }
            4 -> {resultMutableList.add(3,"☆") }
            5 -> {resultMutableList.add(4,"☆") }
        }
        return resultMutableList
    }

    private fun addInfoQuestionsSolved(resultMutableList:MutableList<String>, turn:Int,points:Int){
        if (resultMutableList[turn-1].isEmpty()){
            fileActualUser.appendText("")
            file.appendText("")
        }else{
            fileActualUser.appendText("${turn-1}:${resultMutableList[turn-1]}:$points,")
            file.appendText("${turn-1}:${resultMutableList[turn-1]}:$points,")
        }
    }

    fun addStarsOnMutableList(resultMutableList:MutableList<String>, turn:Int, points:Int){
//        var iteratorStars = 0
//            for (star in resultMutableList){
//                if (star.isEmpty()){
//                    fileActualUser.appendText("")
//                    file.appendText("")
//                }else{
//                    if (iteratorStars == 4){
//                        fileActualUser.appendText("${turn-1}:$star:$points")
//                        file.appendText("${turn-1}:$star:$points")
//                    } else{
//                        fileActualUser.appendText("${turn-1}:$star:$points,")
//                        file.appendText("${turn-1}:$star:$points,")
//                    }
//                }
//                iteratorStars++
//            }
        when(turn){
            1-> {
                addInfoQuestionsSolved(resultMutableList,turn,points)
            }
            2-> {
                addInfoQuestionsSolved(resultMutableList,turn,points)
            }
            3-> {
                addInfoQuestionsSolved(resultMutableList,turn,points)
            }
            4-> {
                addInfoQuestionsSolved(resultMutableList,turn,points)
            }
            5-> {
                addInfoQuestionsSolved(resultMutableList,turn,points)
            }
        }
    }
}
