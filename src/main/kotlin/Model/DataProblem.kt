package Model

data class DataProblem(val name : String, val pass : String, val problemsSolveds : String)
