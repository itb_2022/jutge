package Model

class Preguntes {

    companion object{
        var preguntes = mutableListOf<Pregunta>(
            Pregunta("> [PREGUNTA 1] Estas viatjant en el bus camí al torneig TIMAndorra (Torneig de basketball) juntament amb els teus companys d'equip, quant de sobte no estan fetes les fitxes per poder jugar el primer partit.",
                " ● L'entrenador o el delegat haurà de ferles durant el viatge (el més segur es que es mareixi), t'ha  demanat ajuda a tu, ja que necessita que li donguis les dades dels jugadors del teu equip,\n" +
                        "      de la llista de noms de tot el torneig.",
                " ● A més, es bastant exigent i t'ha dit que segueixis el format donat en el exemple següent.(Recorda que el teu equip és el Valldemia)"
            ),
            Pregunta("> [PREGUNTA 2] Després de ajudar al staff del teu equip a aconseguir la llista de jugadors, s'han donat compte que encara els hi falten unes dades per les fitxes, per això, al haver realitzat un treball tan\n      bò, t'han demanat que els tornis a ajudar.",
            " ● T'han donat una llista de dades (un altre cop sense ordenar), perquè t'ajudi a formatejar correctament la llista anterior de jugadors. Amb la llista de dades d'ajuda, hauràs d'ordenar la\n       llista de jugadors segons el seu número (segons el nom i el primer cognom se't donarà un llista de números).",
            " ● Atenció! La llista de números està desactualitzada i conté números d'altres anys, per això hauràs de comprovar si el jugador està en el torneig."
            ),
            Pregunta("> [PREGUNTA 3] Sembla que finalment podrem jugar, ja que l'staff ha fet el seu treball i ja té les fitxes del equip."+ " Després d'una estona per Andorra, sembla que ha sorgit un nou problema, l'Olga Jubany \n     (La Directora de Valldemia) se li va oblidar fer la reserva de l'hotel, a més, no sabem si els hotels tenen les places necessàries requerides per allotjar un equip de basket tan gran (23 persones).",
            " ● L'Olga ens ha enviat un recull d'hotels, juntament amb les seves places per veure quin es l'idoni per reservar. T'han demanat (com sempre...) que creis una funció, que segons les necessitats,\n     fagi una criba dels hotels.",
            " ● Atenció! Si o si dormiràn en parelles, per tant també hauràs d'afegir a les dades, el total d'habitación utilitzades i la resta entre el total i les utilitzades"
            ),
            Pregunta("> [PREGUNTA 4] Abans del partit, la delagada de pista t'ha dit que es bastant complicat anar portant el compte de punts i faltes del equip per asegurar-se de que l'staff del torneig no te errors,\n      a més, a la delaga l'hi agradaria portar el compte de jugadors que han jugat o no el partit",
            " ● T'han demanat que fagis un petit programa automatitzat per anar apontant tot el que necessiten (punts, faltes i si ha jugat o no cada jugador del equip)",
            " ● No comntenta en que traballis gratis, t'ha dit que has de cumplir certs requisits, han de estar ordenats els jugadors de major a menor número de dorsal y a més has de tenir en compte que si\n      ha anat a la banqueta el jugador o bé ha sortit d'ella, ho hauràs de marcar de la manera específica que t'han demanat (podrás veureu en el input/output) "
            ),
            Pregunta("> [PREGUNTA 5] Heu guanyat el torneig! Pero no tot anava a ser bona sort, heu tingut un altre problema per tornar, després d'aquesta aventura toca revisió i t'han demanat que un paper amb totes\n      les calories que ha menjat cada jugador, les sumis i els hi entreguis",
            " ● Cuidado! La llista esta desordenada, es a dir, s'han anat apuntant a manija que menjava cada jugador, per tant, hauràs de organitzar-ho segons les calories totals, de major a menor",
            " ● Atenció! A última hora t'han dit que hi han algunes dades que son les calories (grams, proteina, hidrats...) i a més t'han dit que els numeros primers tampoc son calories")
        )

        var dataPreguntes = mutableListOf<DataPregunta>(
            DataPregunta( "Castillo Perez,Pol,Boet\n" +
                    "                                        Moreno Martinez,Joan,Valldemia\n"+
                    "                                        Gómez Romero,Jan,UEM\n" +
                    "                                        Moreno Martinez,Pau,Valldemia                  Equip:Valldemia J1:Joan,Moreno,Martinez J2:Pau,Moreno,Martinez J3:Victor,Redrado,Pérez J4:Joan,Ribes,Prats\n" +
                    "                                        Redrado Pérez,Victor,Valldemia\n" +
                    "                                        Sanz Cruz,Jose,Boet\n" +
                    "                                        Ribes Prats,Joan,Valldemia\n" +
                    "                                        Tabares Galinsoga,Pep,UEM",
                "Equip:Valldemia_J1:Fausto,Cuéllar,Cerdán_J2:Lalo,Arrieta,Prats_J3:Joan,Moreno,Martinez_J4:Pau,Moreno,Martinez_J5:Pascual,Alegre,Quesada_J6:Josep,Casals,Somoza_J7:Victor,Redrado,Pérez_J8:Gervasio,Criado,Rico_J9:Tomás,Pons,Sevilla_J10:Pol,Bernabé,Olmo",
                "https://pastebin.com/E3JX59B3"),
            DataPregunta("23:Victor Redrado\n" +
                    "                                        69:Joan Ribes\n" +
                    "                                        3:Pau Moreno\n" +
                    "                                        1:Joel Martinez                    Joel,Martinez->1 Pau,Moreno->3 Victor,Redrado->23 Jan,Saez->45 Joan,Ribes->69 Joan,Roig->76 Victor,Paez->86 Pol,Molina->94\n" +
                    "                                        86:Victor Paez\n" +
                    "                                        94:Pol Molina\n" +
                    "                                        76:Joan Roig\n" +
                    "                                        45:Jan Saez",
                "Joel,Martinez->1_Pau,Moreno->3_Ot,Soler->5_David,Sanchez->11_Victor,Redrado->23_Pol,Perez->32_Jan,Saez->45_Robert,Gisber->54_Joan,Ribes->69_Gonzalo,Atienza->73_Joan,Roig->76_Victor,Paez->86_Pol,Molina->94",
                "https://pastebin.com/PgDL6bWi"),
            DataPregunta("Hotel Montblanc,53 habitacions disponibles(35 dobles)\n" +
                    "                                        Hotel Sant Eloi,21 habitacions disponibles(16 dobles)\n" +
                    "                                        Hotel Pyrénées,8 habitacions disponibles(2 dobles)             Hotel Sant Eloi,16 Habitacions Dobles,4 restants\n" +
                    "                                        Hotel Sol-Park,34 habitacions disponibles(30 dobles)\n" +
                    "                                        Hotel Guillem & Spa,74 habitacions disponibles(11 dobles)",
                "Hotel_Robert,32_Habitacions_Dobles,18_restants",
                "https://pastebin.com/KMzP3Wha"),
            DataPregunta("3:Pau Moreno,3 punts\n" +
                    "                                        69:Joan Ribes,2 punts\n" +
                    "                                        3:Pau Moreno, 3 punts\n" +
                    "                                        3:Pau Moreno, 2 punts                    69:Joan Ribes->5 Punts 3:Pau Moreno->8 Punts 76:Joan Roig->1 Punt 86:Victor Paez->NO HA JUGAT 45:Jan Saez->NO HA JUGAT\n" +
                    "                                        86:Victor Paez, NO HA JUGAT\n" +
                    "                                        69:Joan Ribes,3 punts\n" +
                    "                                        76:Joan Roig,1 punt\n" +
                    "                                        45:Jan Saez, NO HA JUGAT",
                "94:Pol_Molina->NO_HA_JUGAT_86:Victor_Paez->20_76:Joan_Roig->5_69:Joan_Ribes->16_45:Jan_Saez->4_3:Pau_Moreno->13_1:Joel_Martinez->12",
                "https://pastebin.com/fWMn9Udh"),
            DataPregunta("Victor Redrado,345 calories\n" +
                    "                                        Joan Roig, 512 calories \n" +
                    "                                        Pau Moreno, 317 calories\n" +
                    "                                        Pau Moreno, 876 calories                    Victor Redrado=1003,Pau Moreno=876,Joan Roig=1154,Victor Paez=0,Jan Saez=996\n" +
                    "                                        Joan Roig, 642 calories\n" +
                    "                                        Victor Paez, 769 calories\n" +
                    "                                        Victor Redrado,658 calories\n" +
                    "                                        Jan Saez, 996",
                "Victor_Redrado=2668,Pol_Molina=2448,Jan_Saez=2404,Joan_Ribes=1859,Joan_Roig=1783,Joel_Martinez=1446,Pau_Moreno=876,Victor_Paez=0",
                "https://pastebin.com/kfmq6Nca")
        )
    }
}