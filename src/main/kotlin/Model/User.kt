package Model

import Verd
import file
import reset
import java.io.File
import java.nio.file.Files
import java.nio.file.Paths

class User {
    fun getUserNameDB(userLineLogged: Int):String{
        var nameDB = ""
        val lineUserLogged = Files.readAllLines(Paths.get("$file"))[userLineLogged]
        val temp = lineUserLogged.split(";")
        nameDB = temp[0]
        return nameDB
    }
}