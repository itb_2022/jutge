package Model

import file
import panelHistoric
import reset
import java.util.Scanner

class Teacher {
    fun addProblem(scan: Scanner, inputIGN:String){
        println(" Escriu la pregunta/continuació de la historia"+ reset)
        print(inputIGN + " ➜ ")
        val inputQuestion = scan.next()
        println(" Escriu el primer enunciat d'ajuda"+ reset)
        print(inputIGN + " ➜ ")
        val inputHelp1 = scan.next()
        println(" Escriu el segon enunciat d'ajuda"+ reset)
        print(inputIGN + " ➜ ")
        val inputHelp2 = scan.next()
        var newQuestion = Pregunta(inputQuestion,inputHelp1,inputHelp2)
        Preguntes.preguntes.add(newQuestion)
    }

    fun addDataProblem(scan: Scanner, inputIGN:String){
        println(" Escriu l'input i output de la pregunta"+ reset)
        print(inputIGN + " ➜ ")
        val inputAndOutput = scan.next()
        println(" Escriu el resultat"+ reset)
        print(inputIGN + " ➜ ")
        val result = scan.next()
        println(" Escriu el pastebin amb la informació a tractar"+ reset)
        print(inputIGN + " ➜ ")
        val pastebin = scan.next()
        var newDataQuestion = DataPregunta(inputAndOutput,result,pastebin)
        Preguntes.dataPreguntes.add(newDataQuestion)
    }

    fun alumReport(alumName:String){
        var lineUser = 0
        var exactLineUserDB = 0
        var userExists = false
        file.forEachLine {
            var nameUserDB = it.split(";")
            if (alumName == nameUserDB[0]){
                exactLineUserDB = lineUser
                userExists = true
            }
            lineUser++
        }
        panelHistoric(exactLineUserDB, userExists)
    }
}