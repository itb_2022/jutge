@file:Suppress("NAME_SHADOWING")

import Model.*
import Model.Preguntes.Companion.dataPreguntes
import Model.Preguntes.Companion.preguntes
import java.io.File
import java.util.*
import kotlin.system.exitProcess
//COLORS
const val Vermell = "\u001b[31m"
const val Groc = "\u001B[33m"
const val Verd = "\u001B[32m"
const val Blau = "\u001B[36m"
const val reset = "\u001b[0m"
const val VerdSuave = "\u001B[38;5;70m"
//FILES
val file = File("./src/main/Utilities/UserData")
val fileActualUser = File("./src/main/Utilities/ActualUser")

fun main() {
    val scan = Scanner(System.`in`)
    val letsPlay = true
    //INTRODUCTION/INITIAL WELCOME
    println("Benvingut a" + Groc + " JOC DE PROVES" + reset)
    println()
    println("Aquí tens les credencials de professor per poder entrar en el modo "+Groc+"TEACHER"+reset)
    println("user: Jordi")
    println("password: !432asd_")
    println()
    print(Verd+" ▶ INTRODUEIX EL NOM DE JUGADOR: "+reset)
    val inputIGN = scan.next()
    var IGNexists = false
    print(Verd+" ▶ INTRODUEIX LA CONTRASEÑA: "+reset)
    var inputPassword = scan.next()
    var passCorrect = false

    //WE WROTE THE ACTUAL USER TO A NEW FILE
    val actualUser = "$inputIGN;$inputPassword"
    fileActualUser.writeText("$actualUser;")
    //LOGIN SYSTEM
    val dataActualUserSplitted = actualUser.split(";").toMutableList()
    var userExists = false
    var lineUser = 0
    var exactLineUserDB = 0
    var notPlayedYet = false

    //USER AND PASS CORRECTS
    file.forEachLine {
        val dataBaseSplitUsers = it.split(";").toMutableList()
        if ( dataActualUserSplitted[0] == dataBaseSplitUsers[0]){
            IGNexists = true
            if ( dataActualUserSplitted[1] == dataBaseSplitUsers[1]){
                passCorrect = true
                exactLineUserDB = lineUser
                println("$Verd[LOGGED CORRECTED]$reset")
                println()
                notPlayedYet = true
                userExists = true
            }
        }
        lineUser++
    }
    //USER CORRECT BUT PASS INCORRECT
    if (IGNexists == true){
        while (passCorrect == false){
            println("$Vermell[ERROR]$Verd Has introduit malament la contraseña, torna a introduirla$reset")
            inputPassword = scan.next()

            val actualUser = "$inputIGN;$inputPassword"
            fileActualUser.writeText("$actualUser;")
            val dataActualUserSplitted = actualUser.split(";").toMutableList()
            var lineUser = 0

            file.forEachLine {
                val dataBaseSplitUsers = it.split(";").toMutableList()
                if ( dataActualUserSplitted[0] == dataBaseSplitUsers[0]){
                    IGNexists = true
                    if ( dataActualUserSplitted[1] == dataBaseSplitUsers[1]){
                        passCorrect = true
                        exactLineUserDB = lineUser
                        println(Verd+"[LOGGED CORRECTED]"+ reset)
                        println()
                        notPlayedYet = true
                    }
                }
                lineUser++
            }
        }
    }
    //USER DOESN'T EXIST, SO WE JUST ADD THE USER DATA TO THE FILE
    else if (IGNexists == false){
        file.appendText("\n$inputIGN;$inputPassword;")
    }
    //USER IGN AND PASS CORRECTS, SO MEANS THAT USER EXISTS ON THE FILE
    else if (IGNexists == true && passCorrect == true){
        userExists = true
    }

    var questionSelected = 0
    while (letsPlay){
        var goodAnswer = 0
        var badAnswer = false
        var teacherMode = false
        if (inputIGN == "Jordi" && inputPassword == "!432asd_"){
            teacherMode = true
        }
        when(teacherMode){
            //TEACHER MODE
            true -> {
                val nameSavedOnDB = User().getUserNameDB(exactLineUserDB)
                teacherPanel(nameSavedOnDB)
                println(" ▶ Escriu el número de l'acció que vols " + Verd + "portar a terme"+ reset)
                print("$inputIGN ➜ ")
                when(scan.nextInt()){
                    1 ->{
                        println(" ▶ A continuació set demanaràn les dades per afegir el problema a la llista de problemes"+ reset)
                        println(" ▶ Recorda que a part de la pregunta inicial hauràs d'afegir 2 enunciats d'ajuda, on s'aclarin requisits i petits detalls"+ reset)
                        Teacher().addProblem(scan,inputIGN)
                        println(" ▶ A continuació set demanarà els exemples del model de pregunta a seguir, juntament amb la resposta i una llista a pastebin"+ reset)
                        println(" ▶ Recorda que a part de la pregunta inicial hauràs d'afegir 2 enunciats d'ajuda, on s'aclarin requisits i petits detalls"+ reset)
                        Teacher().addDataProblem(scan,inputIGN)
                    }
                    2 ->{
                        println("▶ Introdueix el nom de l'alumne per revisar el seu report "+ reset)
                        println(Vermell+" [ATENCIÓ]"+reset+" Recorda que el nom de l'usuari haurà d'estar a la BD")
                        print(inputIGN + " ➜ ")
                        val alumName = scan.next()
                        Teacher().alumReport(alumName)
                        var teacherInputContine:String
                        do {
                            println("Vols continuar? "+ reset)
                            println("▶ Escriu "+Groc+"SI"+reset+" per continuar"+reset)
                            teacherInputContine = scan.next().uppercase()
                        }while (teacherInputContine != "SI")
                    }
                }
            }
            //NORMAL MODE
            false ->{
                do {
                    initialPanel(questionSelected)
                    println(" ▶ Escriu el número de l'acció que vols " + Verd + "portar a terme"+ reset)
                    print(inputIGN + " ➜ ")
                    val inputActionPanel = scan.nextInt()
                    println()
                    when(inputActionPanel){
                        1 ->{
                            panelQuestions()
                            var actionToDo: Int
                            println(Verd+"▶ Escriu el " + Groc + "NÚMERO " + Verd + "per seleccionar la pregunta o bé per sortir" + reset)
                            print(inputIGN + " ➜ ")
                            val inputAction = scan.nextInt()
                            actionToDo = inputAction-1
                            if (inputAction != 9){
                                println()
                                problema(actionToDo)
                                pregunta()
                                println()
                                print(inputIGN + " ➜ ")
                                val inputResolveOrNot = scan.nextInt()
                                println(inputResolveOrNot)
                                var state = false
                                var trys = 0
                                var formatedAnswer:String
                                var resultMutableList = MutableList(4){""}
                                if (inputResolveOrNot == 1){
                                    infoProblemes(actionToDo)
                                    println(VerdSuave+" ▶ Envia la teva resposta ➜"+reset)
                                    var inputProblemAnswer:String
                                    do {
                                        inputProblemAnswer = scan.nextLine()
                                    }while (inputProblemAnswer == "")

                                    //WE FORMAT OUR ANSWER, REMOVING SPACES FOR DASHES
                                    formatedAnswer = Judge().formatAnswer(inputProblemAnswer)

                                    while (formatedAnswer != dataPreguntes[actionToDo].Result && trys != 4){
                                        if (trys < 4){
                                            println(Verd+"Et queden "+Vermell+"${4-trys}"+Verd+" intents")
                                        }
                                        println(Vermell+ "[ERROR]"+VerdSuave+" ▶ Envia un altre cop la resposta ➜"+reset)
                                        inputProblemAnswer = scan.nextLine()
                                        formatedAnswer = Judge().formatAnswer(inputProblemAnswer)
                                        trys++
                                    }
                                    if (trys == 4){
                                        resultMutableList = Judge().addStarsIfNotSolved(actionToDo+1,resultMutableList)
                                        println("Has perdut")
                                    }
                                    else if (formatedAnswer == dataPreguntes[actionToDo].Result){
                                        resultMutableList = Judge().addStarsIfSolved(actionToDo+1,resultMutableList)
                                        goodAnswer++
                                        state = true
                                    }
                                } else{
                                    resultMutableList = Judge().addStarsIfNotSolved(actionToDo+1,resultMutableList)
                                    badAnswer = true
                                }
                                statsProblema(actionToDo, state, trys+1)
                                notPlayedYet = true

                                var points:Int
                                if (inputResolveOrNot == 1){
                                    if (badAnswer){
                                        points = 0
                                        badAnswer = false
                                    }else{
                                        points = Judge().puntuation(trys+1)
                                    }

                                }else{
                                    points = 0
                                }
                                Judge().addStarsOnMutableList(resultMutableList, inputAction+1, points)
                                questionSelected = actionToDo
                            }
                        }
                        2 ->{
                            if (notPlayedYet == false){
                                println()
                                println(Vermell+ "[ATENCIÓ]"+VerdSuave+" Abans de veure el teu historial, hauràs de jugar, ja que aquest usuari no te registres anteriors")
                                println("Ho has entès?")
                                do {
                                    println(reset+"Escriu SI per continuar")
                                    val inputUserYes = scan.next().uppercase()
                                }while (inputUserYes != "SI")
                            }else{
                                panelHistoric(exactLineUserDB, userExists)
                                do {
                                    println("Aquí tens el teu històric \nEscriu SI per continuar")
                                    val inputUserYes = scan.next().uppercase()
                                }while (inputUserYes != "SI")
                            }
                        }
                        3 ->{
                            menuPanel()
                            do {
                                println(VerdSuave+"▶ Si has acabat amb el menu, escriu " + Groc + "ACABAR " + VerdSuave + "per tornar al submenú" + reset)
                                print(inputIGN + " ➜ ")
                                val inputHelpFinished = scan.next().uppercase()
                                println()
                            }while (inputHelpFinished != "ACABAR")
                        }
                        4 ->{
                            var game = true
                            var turn:Int
                            var resultMutableList = MutableList(4){""}
                            println()
                            println(Verd+"JOC DE PROVES | Camí al torneig"+reset)
                            while (game){
                                turn = 1
                                val problemsSolvedState = Judge().checkProblemsSolvedByUser(exactLineUserDB, userExists)
                                for (problem in problemsSolvedState){
                                    if (problem == "✅" || problem == "❌"){
                                        turn++
                                    }
                                    else if (problem == "❓"){
                                    }
                                }
                                problema(turn-1)
                                pregunta()
                                print(inputIGN + " ➜ ")
                                val inputResolveOrNot = scan.nextInt()
                                println(inputResolveOrNot)
                                var state = false
                                var trys = 0
                                var formatedAnswer:String
                                if (inputResolveOrNot == 1){
                                    infoProblemes(turn-1)
                                    trys=1
                                    println(VerdSuave+" ▶ Envia la teva resposta ➜"+reset)
                                    var inputProblemAnswer:String
                                    do {
                                        inputProblemAnswer = scan.nextLine()
                                    }while (inputProblemAnswer == "")
                                    formatedAnswer = Judge().formatAnswer(inputProblemAnswer)//WE FORMAT OUR ANSWER, REMOVING SPACES FOR DASHES
                                    while (formatedAnswer != dataPreguntes[turn-1].Result && trys != 4){
                                        if (trys < 4){
                                            println(Verd+"Et queden "+Vermell+"${4-trys}"+Verd+" intents")
                                        }
                                        println(Vermell+ "[ERROR]"+VerdSuave+" ▶ Envia un altre cop la resposta ➜"+reset)
                                        inputProblemAnswer = scan.nextLine()
                                        formatedAnswer = Judge().formatAnswer(inputProblemAnswer)
                                        trys++
                                    }
                                    if (trys == 4){
                                        resultMutableList = Judge().addStarsIfNotSolved(turn,resultMutableList)
                                        println("Has perdut")
                                    }
                                    else if (formatedAnswer == dataPreguntes[turn-1].Result){
                                        resultMutableList = Judge().addStarsIfSolved(turn,resultMutableList)
                                        goodAnswer++
                                        state = true
                                    }
                                } else{
                                    resultMutableList = Judge().addStarsIfNotSolved(turn,resultMutableList)
                                    badAnswer = true
                                }
                                statsProblema(turn, state, trys)
                                notPlayedYet = true
                                //turn++
                                println("A continuació s'enviará el següent problema, estas d'acord?"+reset)
                                println(" ▶ Escriu " + Verd + "1" + reset + " per continuar ")
                                println(" ▶ Escriu " + Verd + "2" + reset + " per acabar aquí i guardar ")
                                print(inputIGN + " ➜ ")
                                val nextQuestion = scan.nextInt()
                                var points:Int
                                if (nextQuestion == 1){
                                    if (badAnswer){
                                        points = 0
                                        badAnswer = true
                                    }else{
                                        points = Judge().puntuation(trys)
                                    }
                                    Judge().addStarsOnMutableList(resultMutableList, turn, points)
                                }else{
                                    points = 0
                                }
                                if (nextQuestion == 2){
                                    Judge().addStarsOnMutableList(resultMutableList, turn, points)
                                    notPlayedYet = true
                                    game = false
                                }
                                if(turn==5){
                                    println()
                                    println(Verd+"HAS ACABAT! \uD83C\uDF89 \uD83C\uDF89"+reset)
                                    println(Verd+"Espero que t'hagis divertit!")
                                    game = false
                                    println()
                                    println(reset + "Vols tornar a jugar? Escriu SI o NO")
                                    val inputContinueOrNot = scan.next().uppercase()
                                    if (inputContinueOrNot == "NO"){
                                        exitProcess(1)
                                    }
                                    else if (inputContinueOrNot == "SI"){
                                    }
                                }
                            }
                        }
                        5 ->{
                            exitProcess(1)
                        }
                    }
                }while (inputActionPanel != 1 && inputActionPanel != 2 && inputActionPanel != 3 && inputActionPanel != 4 && inputActionPanel != 5)
            }
        }
    }
}
fun initialPanel(questionSelected:Int){
    println()
    println("┏━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┓")
    println("┃              PANELL INICIAL             ┃")
    println("┃                                         ┃")
    println("┃        1 -> LLISTA DE PROBLEMES         ┃")
    println("┃        2 -> HISTÒRIC PROBLEMES          ┃")
    println("┃        3 -> AJUDA                       ┃")
    println("┃        4 -> JUGAR                       ┃")
    println("┃        5 -> SORTIR                      ┃")
    println("┃                                         ┃")
    println("┗━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┛")
    println("     HAS SELECCIONAT LA PREGUNTA $questionSelected")
    println()
}
fun teacherPanel(nameSavedOnDB: String){
    println("┏━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┓")
    println("         PANELL PROFESSOR [$nameSavedOnDB]")
    println("                                         ")
    println("        1 -> AFEGIR PROBLEMA             ")
    println("        2 -> REPORT ALUMNE               ")
    println("                                         ")
    println("┗━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┛")
    println()
}
fun menuPanel(){
    println()
    println("┏━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┓")
    println("┃              PANELL D'AJUDA             ┃")
    println("┃                                         ┃")
    println("┃      Aquest joc és una secuència de 5   ┃")
    println("┃   preguntes, les quals presentaràn una  ┃")
    println("┃             serie de problemes.         ┃")
    println("┃                                         ┃")
    println("┃        L'usuari, mitjançant unes        ┃")
    println("┃     dades que se l'hi donaràn, haurà    ┃")
    println("┃   d'introduir el resultat a la màquina. ┃")
    println("┃                                         ┃")
    println("┗━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┛")
    println()
}
fun panelQuestions(){
    println()
    println("┏━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┓")
    println("┃             "+Verd+"PANELL PREGUNTES"+reset+"            ┃")
    println("┃                                         ┃")
    println("┃    "+Verd+"Quina pregunta vols visualitzar?"+reset+"     ┃")
    println("┃    "+Vermell+"1"+Verd+" ->"+reset+" Seleccionar jugadors del equip  ┃")
    println("┃    "+Vermell+"2"+Verd+" ->"+reset+" Ordenar jugadors del equip      ┃")
    println("┃    "+Vermell+"3"+Verd+" ->"+reset+" Seleccionar hotel disponible    ┃")
    println("┃    "+Vermell+"4"+Verd+" ->"+reset+" Contador automatic              ┃")
    println("┃    "+Vermell+"5"+Verd+" ->"+reset+" Calories consumides             ┃")
    println("┃                                         ┃")
    println("┃    "+Vermell+"9"+Verd+" ->"+reset+" Sortir                          ┃")
    println("┃                                         ┃")
    println("┗━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┛")
}
fun panelHistoric(userLineLogged:Int, UserExist: Boolean){
    val problemsSolvedStateFromUser = Judge().checkProblemsSolvedByUser(userLineLogged, UserExist)
    val pointsFromUser = Judge().checkPoints(userLineLogged, UserExist)
    println()
    println("┏━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┓")
    println("                    PANELL HISTÒRIC            ")
    println("                                         ")
    println("      Aquí tens l'historic de preguntes   ")
    println("      1r Model.Pregunta ->  ${VerdSuave}Estat:${reset} ${problemsSolvedStateFromUser[0]} ${VerdSuave}Points:${reset} ${pointsFromUser[0]} ")
    println("      2n Model.Pregunta ->  ${VerdSuave}Estat:${reset} ${problemsSolvedStateFromUser[1]} ${VerdSuave}Points:${reset} ${pointsFromUser[1]} ")
    println("      3r Model.Pregunta ->  ${VerdSuave}Estat:${reset} ${problemsSolvedStateFromUser[2]} ${VerdSuave}Points:${reset} ${pointsFromUser[2]} ")
    println("      4t Model.Pregunta ->  ${VerdSuave}Estat:${reset} ${problemsSolvedStateFromUser[3]} ${VerdSuave}Points:${reset} ${pointsFromUser[3]} ")
    println("      5é Model.Pregunta ->  ${VerdSuave}Estat:${reset} ${problemsSolvedStateFromUser[4]} ${VerdSuave}Points:${reset} ${pointsFromUser[4]} ")
    println("                                         ")
    println("┗━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┛")
}
fun pregunta() {
    println("                                $Vermell┏━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┓$Groc                                                         _____________$reset")
    println("                                "+Vermell+"┃          "+reset+"VOLS RESOLDRE"+Vermell+"             ┃       "+reset+"▶ Introdueix '"+Verd+"1"+reset+"' per resoldre'l" +Groc+ "                 _/_| [][][][][]|"+reset)
    println("                                "+Vermell+"┃         "+reset+"AQUEST PROBLEMA?"+Vermell+"           ┃       "+reset+"▶ Introdueix '"+Verd+"2"+reset+"' per passar al següent" +Groc+ "        ☾                |"+reset)
    println("                                $Vermell┗━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┛$Groc                                                       ￣OO￣￣￣￣￣OO￣=°°°$reset")
}
fun problema(turn:Int){
    println("$Vermell ┏━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┓$reset")
    println("    ${preguntes[turn].question}")
    println()
    println("    ${preguntes[turn].help1}")
    println()
    println("    ${preguntes[turn].help2}")
    println("$Vermell ┗━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┛$reset")
}
fun infoProblemes(turn:Int){
    var spaces = ""
    when(turn){
        0 ->{
            spaces = "            "
        }
        1 ->{}
        2 ->{
            spaces =  "                            "
        }
        3 ->{}
        4 ->{}
    }
    println("                              $reset     > Exemple")
    println("                              $Vermell          Entrada                $spaces            Sortida")
    println("                              "+Verd+"          ${dataPreguntes[turn].Input}   ")
    println()
    println(reset+"\uD83D\uDD17 Llista:${dataPreguntes[turn].pastebinData}   ")
}
fun statsProblema(turn:Int, state:Boolean, trys:Int){
    val stateMessage: String
    if (state){ stateMessage = VerdSuave+"RESOLT" } else stateMessage = Vermell+"NO RESOLT"
    println()
    println("                                                                      $Vermell┏━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┓")
    println("                                                                      $Vermell┃            "+Verd+"STATS PROBLEMA "+Vermell+"#$turn            ┃")
    println("                                                                      $Vermell┗━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┛")
    println("                                                                    $reset       > INTENTS -> $trys")
    println("                                                                    $reset       > ESTAT   -> $stateMessage$reset")
    println()
}